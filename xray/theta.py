from numpy import arcsin,rad2deg

alpha = 0.0709 * pow(10,-9)
beta = 0.0632 * pow(10,-9)
d = 2.82 * pow(10,-10)


kb_m1 = rad2deg(arcsin(1*beta/(2*d)))
kb_m2 = rad2deg(arcsin(2*beta/(2*d)))
kb_m3 = rad2deg(arcsin(3*beta/(2*d)))
ka_m1 = rad2deg(arcsin(1*alpha/(2*d)))
ka_m2 = rad2deg(arcsin(2*alpha/(2*d)))
ka_m3 = rad2deg(arcsin(3*alpha/(2*d)))
ka_m4 = rad2deg(arcsin(4*alpha/(2*d)))

print("Kb_m1 : " + str(kb_m1))
print("Kb_m2 : " + str(kb_m2))
print("Kb_m3 : " + str(kb_m3))
print("Ka_m1 : " + str(ka_m1))
print("Ka_m2 : " + str(ka_m2))
print("Ka_m3: " + str(ka_m3))
print("Ka_m4 : " + str(ka_m4))
