import csv



M = 1.00
Merr = 0.00

C = -0.47
Cerr = 0.02





Angles = []
Errors = []
Imps = []


file_name = 'data/30-kV2'

with open(file_name) as csvfile:
    reader = csv.reader(csvfile, delimiter = '\t')
    for row in reader:
        angle = float(row[0]) * M + C
        err = float(row[0]) * Merr + Cerr
        Angles.append(angle)
        Errors.append(err)
        Imps.append(float(row[1]))



with open(file_name+'calibrated','w') as outfile:
    for i in range(0,len(Angles)):
        outfile.write(str(Angles[i]) + '\t' + str(Imps[i]) + '\t' + str(Errors[i]) + '\n')
