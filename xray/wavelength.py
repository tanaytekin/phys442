import csv
import math
from numpy import deg2rad






Wavelengths = []
Imps = []
Errors = []


d = 2.82 * pow(10,-10) 

file_name = "data/30-kV2calibrated"




with open(file_name) as csvfile:
    reader = csv.reader(csvfile, delimiter = '\t')
    for row in reader:
        angle = float(row[0]) 
        err = float(row[2])

        wavelength = 2*d*math.sin(deg2rad(angle))
        err_wavelength = math.sqrt((2*d*math.cos(deg2rad(angle))*err)**2)


        Wavelengths.append(wavelength)
        Errors.append(err_wavelength)
        Imps.append(float(row[1]))



with open(file_name + '-wavelength' ,'w') as outfile:
    for i in range(0,len(Wavelengths)):
        outfile.write(str(Wavelengths[i]) + '\t' + str(Imps[i]) + '\t' + str(Errors[i]) + '\t0\n')
