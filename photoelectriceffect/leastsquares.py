from math import sqrt

#X = [5.19*pow(10,14), 5.49 *pow(10,14), 6.02 *pow(10,14), 6.88 *pow(10,14), 7.41 *pow(10,14)]
#Y = [0.397,0.541, 0.417, 1.04,1.15]
#Err = [0.026, 0.035,0.029, 0.16, 0.12]


X = [5.19*pow(10,14), 5.49 *pow(10,14), 6.88 *pow(10,14), 7.41 *pow(10,14)]
Y = [0.397,0.541, 1.04,1.15]
Err = [0.026, 0.035, 0.16, 0.12]

def calculate(x,y,err):
    e_1 = 0
    ey_1 = 0
    ex_1 = 0
    ex_2 = 0
    exy_1 = 0
    for i in range(0,len(x)):
        e_1 = e_1 + (1/err[i])**2
        ey_1 = ey_1 + y[i]/(err[i]**2)
        ex_1 = ex_1 + x[i]/(err[i]**2)
        ex_2 = ex_2 + (x[i]**2)/(err[i]**2)
        exy_1 = exy_1 + x[i]*y[i]/(err[i]**2)



    D =  -(ex_1**2) + ex_2 * e_1
    M = (e_1*exy_1 -ex_1*ey_1)/D;
    C = (ex_2*ey_1 -ex_1*exy_1)/D;
    
    Cerr = sqrt(ex_2/D)
    Merr = sqrt(e_1/D)

    print("M = " +str(M)+"\t+/-\t"+str(Merr)) 
    print("C = " +str(C)+"\t+/-\t"+str(Cerr)) 

calculate(X,Y,Err)
