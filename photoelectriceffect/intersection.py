import math



def calculate(c1, c1e, m1, m1e, c2, c2e, m2, m2e):
    v_stop = ((c1-c2)/(m2-m1))
    v_error = math.sqrt(((1/(m1-m2))*c2e)**2 + ((1/(m1-m2))*c1e)**2 + (((c2-c1)/((m1-m2)**2))*m1e)**2 + (((c2-c1)/((m1-m2)**2))*m2e)**2)
    print("V:"+ str(v_stop) + " +/- " + str(v_error))

p0_1 = 25.7
p0_1_e = 2.8
p1_1 = -26.6
p1_1_e = 3.2

p0_2 = -1.56
p0_2_e = 0.31
p1_2 = -0.28
p1_2_e = 0.14





calculate(p0_1,p0_1_e,p1_1,p1_1_e,p0_2,p0_2_e,p1_2,p1_2_e)
