
\documentclass[reprint,amsmath,aps,nofootinbib]{revtex4-2}

\usepackage{graphicx}
\usepackage{dcolumn}
\usepackage{url}
\usepackage{siunitx}
\usepackage{float}
\AtBeginDocument{\renewcommand{\natexlab}[1]{#1}}% <--- the fix
\begin{document}

\title{Photoelectric Effect}
\author{Tan Aytekin}
\email[]{tan.aytekin@boun.edu.tr}
\affiliation{Physics Department, Bogazici University, Istanbul, Turkey}
\collaboration{Partner : Özgür Aydın}
\date{\today}


\begin{abstract}
In this experiment, we tried to observe the phenomenon called photoelectric effect by separating light into colors and reflecting it on a certain material. Then, when we observed current, we applied stoping voltage and using given frequencies of colors and charge of electron we tried to determine planck constant $h$ with a linear regression method. We found $h$ as $(5.71 \pm 0.77)10^{-34}\si{\joule\second}$ which is not perfect but compared to the condition of the setup, it is very acceptable.
\end{abstract}

\maketitle

\section{Introduction}
Photoelectric effect is a phenomenon that when some electromagnetic wave like visible light hits a material it emits electrons or charge carriers. In classical electromagnetism we expect to see that intensity of light affects electrons' kinetic energy. However, in photoelectric effect we see that kinetic energy of electrons only depends on frequency of light which shows that energy specturum of light is not continuous but some discrete packets. This phenomenon shows particle nature of light very well. 

\subsection{History}
Photoelectric effect first discovered by Heinrich Rudolf Hertz in 1887. In one experiment, he realized that ultaviolet light affects the potential to create sparks between two metal plates. Later, Philipp Lenard showed electrically charged particles are emitted from a metal surface when light hits them in 1902.\cite{a_history} Finally, in 1905, Einstein proposed idea of energy quanta and showed quantum nature of the light in his nobel prize winning article.\cite{history2}  
\section{Theory}
According to Planck–Einstein relation energy of a photon given by:
\begin{align}
  E = h\nu 
\end{align}
Assuming light falls on cathode and emits electrons to anode. If we apply reverse potential in order to cut the current flow by creating a reverse electric field, emitted electrons cannot reach the anode. For this reason, energy of photon should equal to sum of work function $W$\footnote{Work function is the minimum thermodynamic work needed to remove an electron from a solid to a point in the vacuum immediately outside the solid surface. \cite{work}} of anode metal\footnote{Because of work function of anode is usually higher than work function of cathode, y-intercept gives work function of anode\cite{Melissinos}} and electrical potential energy.
\begin{align}
  h\nu = qV + W
\end{align}
We can get stopping voltage:
\begin{align}
  V = \frac{h\nu}{q} - \frac{W}{q} \label{eq:3}
\end{align}
If we know the charge of electron and frequencies of colors we used, we can determine $h$ and W with linear regression:
\begin{align}
  \text{Slope} = \frac{h}{q} \hspace{5mm} \text{y-intercept} = -\frac{W}{q}
\end{align}
\section{Experiment}
\subsection{Apparatus and Setup}
Our experimental setup consists a high pressure mercury lamp for light source, black triangular box which includes photocell and prism as showed as Fig.~\ref{fig:box} to separate colors.
\begin{figure}[H]

  \includegraphics[width=\columnwidth]{photos/setup1.jpg}
  \caption{General view of the apparatus}
   \label{fig:app}
\end{figure}



\begin{figure}[H]

  \includegraphics[width=\columnwidth]{photos/inside.jpg}
  \caption{Light is sent though the lens to the left of the box, then light refracted by prism and reflected from the mirror to the photocell.}
   \label{fig:box}
\end{figure}

Photocell current gets amplified by order of $10^{10}$ while stoping voltage applied to photocell and both current and voltage measured with multimeter like Fig.~\ref{fig:circuit}\cite{Gulmez}.  
 
\begin{figure}[H]

  \includegraphics[width=\columnwidth]{photos/circuit.png}
  \caption{Our setup pretty much same as this setup\cite{Gulmez} except we used digital multimeters to measure current and voltage.}
   \label{fig:circuit}
\end{figure}

\subsection{Procedure}
For each color, the mirror inside the box has been adjusted so that the specific color falls on the cathode. Then, again for each color, stoping voltage applied from $0$ to $2.5\si{\volt}$ with steps respect to the change of readings from photocell current. All photocell current and voltage recorded while increasing the voltage.


\section{Data Analysis}
Notice that when we plot stopping voltage versus current look Fig.~\ref{fig:yellow_general}, we see an exponential decay in a short range where the current is less than zero. Theoretically, x-intercept should give us stopping voltage. However, even if stopping voltage prevents electrons from reacing the anode, it doesnt affect whether electrons are emitted from cathode or not, which explains why there is an exponential decay right there.
\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/yellow_general.eps}
  \caption{Photocell current and stopping voltage readings. Notice that exponential decay when current is less than zero. Note that photocell current amplified by $10^{10}$ in order to measure properly. At each data point, error bars are drawn.}
   \label{fig:yellow_general}
\end{figure}

To find the stopping voltage, we need to apply line regression for positive and negative current values for each color and find the intersection point of these lines.

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/yellow.eps}
  \caption{Intersection point of green(Fig.~\ref{fig:yellow_positive}) and blue(Fig.~\ref{fig:yellow_negative}) lines gives us stopping voltage for yellow color.}
   \label{fig:yellow}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/yellow_positive.eps}
  \caption{Line regression for positive current values for yellow color. At each data point, error bars are drawn.}
   \label{fig:yellow_positive}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/yellow_negative.eps}
  \caption{Line regression for negative current values for yellow color. At each data point, error bars are drawn.}
   \label{fig:yellow_negative}
\end{figure}


\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/green.eps}
  \caption{Intersection point of green(Fig.~\ref{fig:green_positive}) and blue(Fig.~\ref{fig:green_negative}) lines gives us stopping voltage for green color.}
   \label{fig:green}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/green_positive.eps}
  \caption{Line regression for positive current values for green color. At each data point, error bars are drawn.}
   \label{fig:green_positive}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/green_negative.eps}
  \caption{Line regression for negative current values for green color. At each data point, error bars are drawn.}
   \label{fig:green_negative}
\end{figure}


\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/cyan.eps}
  \caption{Intersection point of green(Fig.~\ref{fig:cyan_positive}) and blue(Fig.~\ref{fig:cyan_negative}) lines gives us stopping voltage for cyan color.}
   \label{fig:cyan}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/cyan_positive.eps}
  \caption{Line regression for positive current values for cyan color. At each data point, error bars are drawn.}
   \label{fig:cyan_positive}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/cyan_negative.eps}
  \caption{Line regression for negative current values for cyan color. At each data point, error bars are drawn.}
   \label{fig:cyan_negative}
\end{figure}


\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/blue.eps}
  \caption{Intersection point of green(Fig.~\ref{fig:blue_positive}) and blue(Fig.~\ref{fig:blue_negative}) lines gives us stopping voltage for blue color.}
   \label{fig:blue}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/blue_positive.eps}
  \caption{Line regression for positive current values for blue color. At each data point, error bars are drawn.}
   \label{fig:blue_positive}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/blue_negative.eps}
  \caption{Line regression for negative current values for blue color. At each data point, error bars are drawn.}
   \label{fig:blue_negative}
\end{figure}


\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/violet.eps}
  \caption{Intersection point of green(Fig.~\ref{fig:violet_positive}) and blue(Fig.~\ref{fig:violet_negative}) lines gives us stopping voltage for violet color.}
   \label{fig:violet}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/violet_positive.eps}
  \caption{Line regression for positive current values for violet color. At each data point, error bars are drawn.}
   \label{fig:violet_positive}
\end{figure}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/violet_negative.eps}
  \caption{Line regression for negative current values for violet color. At each data point, error bars are drawn.}
   \label{fig:violet_negative}
\end{figure}


\begin{table}[H]
\caption{\label{tab:lines}%
All linear regressions for all 5 colors. Slope is $m$ with error $\sigma_m$ and y-intercept is $c$ with error $\sigma_c$ and $\chi_\nu^2$ per degree of freedom $\nu$ is given.
}
\begin{ruledtabular}
\begin{tabular}{cccccc}
\textrm{} &
\textrm{\textit{$m$}\hspace{1mm}[$\si{\milli\ampere}/\si{\volt}$]} &
\textrm{$\sigma_m$\hspace{1mm}[$\si{\milli\ampere}/\si{\volt}$]} &
\textrm{$c$\hspace{1mm}[$\si{\milli\ampere}$]} &
\textrm{$\sigma_c$\hspace{1mm}[$\si{\milli\ampere}$]} &   
\textrm{$\chi_\nu^2$}\\                        
\colrule
$yellow^+$   & -16.67  & 0.94  & 5.71 &  0.20 & 0.426      \\
$yellow^-$   & -0.127  & 0.053  & -0.863 &  0.095  &  0.012     \\
\hline 
$green^+$   & -25.6  & 1.3  & 12.07 &  0.53   & 0.125    \\
$green^-$   & -0.237  & 0.071  & -1.65 &  0.13 & 0.040      \\
\hline 
$cyan^+$   & -7.31  & 0.44  & 2.894 &  0.050 & 0.215       \\
$cyan^-$   & -0.052  & 0.046  & -0.133 &  0.087 & 0.009       \\
\hline 
$blue^+$   & -26.6  & 3.2  & 25.7 &  2.8 & 0.234      \\
$blue^-$   & -0.28  & 0.14  & -1.56 &  0.31 &  0.196       \\
\hline 
$violet^+$   & -12.14  & 0.94  & 13.25 &  0.84 & 0.084      \\
$violet^-$   & -0.259  & 0.097  & -0.40 &  0.20 & 0.091       \\
\end{tabular}  
\end{ruledtabular}
\end{table}




Intersection of two lines can be easily calculated:
\begin{align}
L_1 : y=m_1x+c_1 \\  
L_2 : y=m_2x+c_2 \\
x = \frac{c_1-c_2}{m_2-m_1}
\end{align}
with error propogation:

\scriptsize{
\begin{align}
\sigma_x  &= \sqrt{ \left( \frac {\partial{x}}{\partial{c_1}} \right)^2\sigma_{c1}^2   +  \left( \frac {\partial{x}}{\partial{c_2}} \right)^2\sigma_{c2}^2  + \left( \frac {\partial{x}}{\partial{m_1}} \right)^2\sigma_{m1}^2  + \left( \frac {\partial{x}}{\partial{m_2}} \right)^2\sigma_{m2}^2} \\
&=\sqrt{ \left( \frac{\sigma_{c1}}{m2-m1} \right)^2 +  \left( \frac{\sigma_{c2}}{m2-m1} \right)^2 +  \left( \frac{(c_1-c_2)\sigma_{m1}}{(m2-m1)^2} \right)^2  +  \left( \frac{(c_1-c_2)\sigma_{m1}}{(m2-m1)^2} \right)^2 }
\end{align}}
\normalsize

We get stopping voltages:
\begin{align*}
  V_{yellow} &= 0.397 \pm 0.026 \si{\volt}\\
  V_{green} &= 0.541 \pm 0.035 \si{\volt}\\
  V_{cyan} &= 0.417 \pm 0.029 \si{\volt}\\
  V_{blue} &= 1.04 \pm 0.16 \si{\volt}\\
  V_{violet} &= 1.15 \pm 0.12 \si{\volt}\\
\end{align*}

With the given\cite{Gulmez} color frequencies:
\begin{align*}
  f_{yellow} = 5.19 \times 10^{14} \si{\hertz}\\
  f_{green} = 5.49 \times 10^{14} \si{\hertz}\\
  f_{cyan} = 6.02 \times 10^{14} \si{\hertz}\\
  f_{blue} = 6.88 \times 10^{14} \si{\hertz}\\
  f_{violet} = 7.41 \times 10^{14} \si{\hertz}\\
\end{align*}

\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/final.eps}
  \caption{Linear regression with least squares method for frequency vs stopping voltage.}
   \label{fig:final}
\end{figure}

Using least squares method, slope and y-intercept can be found:

\begin{table}[H]
\caption{\label{tab:lines}%
Linear regression with least squares method for ~Eqn \ref{eq:3} where m is slope with error $\sigma_m$ and c is y-intercept with error $\sigma_c$ and $\chi_\nu^2$ per degree of freedom $\nu$ is given
}
\begin{ruledtabular}
\begin{tabular}{ccccc}
\textrm{\textit{$m$}\hspace{1mm}[$10^{-15}\si {\joule\second/\coulomb}$]} &
\textrm{$\sigma_m$\hspace{1mm}[$10^{-15}\si {\joule\second/\coulomb}$]} &
\textrm{$c\hspace{1mm}[$\si {\volt}$]$} &
\textrm{$\sigma_c$\hspace{1mm}[$\si {\volt}$]$$} &
\textrm{$\chi_\nu^2$} \\          
\colrule
1.65  & 0.36  & -0.46 &  0.20 & 39.09      \\
\end{tabular}  
\end{ruledtabular}
\end{table}



\begin{align*}
\text{Slope} = \frac{h}{q}  = (1.65 \pm 0.36)\times 10^{-15} \si {\joule\second/\coulomb}
\end{align*}
Multiplying with given electron charge $1.60 \times 10^{-19} \si{\coulomb}$ we get:
\begin{align*}
h = (2.64 \pm 0.58)\times 10^{-34} \si{\joule\second}
\end{align*}
which is very bad result.
\begin{figure}[H]

  \includegraphics[width=\columnwidth]{graphs/final_without_cyan.eps}
  \caption{Linear regression with least squares method for frequency vs stopping voltage without cyan color.}
   \label{fig:final_without_cyan}
\end{figure}

Since cyan was hard to identify, let's calculate without cyan color:

\begin{table}[H]
\caption{\label{tab:lines}%
Linear regression with least squares method for ~Eqn \ref{eq:3} without cyan color, where m is slope with error $\sigma_m$ and c is y-intercept with error $\sigma_c$ and $\chi_\nu^2$ per degree of freedom $\nu$ is given
}
\begin{ruledtabular}
\begin{tabular}{ccccc}
\textrm{\textit{$m$}\hspace{1mm}[$10^{-15}\si {\joule\second/\coulomb}$]} &
\textrm{$\sigma_m$\hspace{1mm}[$10^{-15}\si {\joule\second/\coulomb}$]} &
\textrm{$c\hspace{1mm}[$\si {\volt}$]$} &
\textrm{$\sigma_c$\hspace{1mm}[$\si {\volt}$]$$} &
\textrm{$\chi_\nu^2$} \\          
\colrule
3.57  & 0.48  & -1.44 &  0.26 & 0.48      \\
\end{tabular}  
\end{ruledtabular}
\end{table}


\begin{align*}
\text{Slope} = \frac{h}{q}  = (3.57 \pm 0.48)\times 10^{-15} \si {\joule\second/\coulomb} \\
\text{y-intercept} = -\frac{W}{q} = (-1.44 \pm 0.26)\si{\volt} \\
h = (5.71 \pm 0.77)\times 10^{-34} \si{\joule\second} \\
W = (2.30 \pm 0.42)\times 10^{-19} \si{\joule}
\end{align*}

gives reasonable results.

\section{Conclusion and Possible Error Sources}
\subsection{Conclusion}
Without cyan color we found $h$ as $(5.71 \pm 0.77)\times 10^{-34} \si{\joule\second}$ which is about $1.2\times\sigma$ away from accepted value $6.62\times 10^{-34} \si{\joule\second}$ which can be considered as a good result.

If we look at work function we found as $(2.30 \pm 0.42)\times 10^{-19} \si{\joule} \approx (1.43+0.26)\si{\electronvolt}$. Comparison with the work function of some metals\cite{work}, our value is quite low. The closest value to our finding belongs to the Cessium metal with $1.95\si{\electronvolt}$. However, Sodium with $2.36\si{\electronvolt}$ and Potassium with $2.29\si{\electronvolt}$ are very close to Cessium. Thus, hard to say that it is definitely Cessium.
\subsection{Possible Error Sources}

According to Millikan's Paper\cite{Millikan}, reflected light through walls can lead to false readings, since unamplified photocell current readings are too small, reflected lights other than our source can lead problems. Another problem is that the photocell-voltage does not depend on the intensity of the light. Measuring with the different light intensities can lead to different photocell currents, which can affect our calculations.\\
Lastly, the given frequencies of light may not be exactly the same as our colors' frequencies since we choose colors only with our eyes.


\appendix
\section{Mathematical Concepts\cite{Gulmez2}}

\subsection{Chi-squared Test}
$\chi^2$ can be defined as:
\begin{align}
  \chi^2_\nu = \sum \frac{(y_i -y(x_i))^2}{\sigma^2_i}
\end{align}
$\chi^2$ per degrees of freedom:
\begin{align}
  \chi^2_\nu = \frac{1}{\nu} \sum \frac{(y_i -y(x_i))^2}{\sigma^2_i}
\end{align}
where $\nu$ is the number of degrees of freedom; the number of data points minus the number of parameters: $N-m$.

\subsection{Least Squares Method}

Let's define line for fitting as $y_i = a_0 + a_1x_i$
Parameters are
\begin{align}
  a_0 &= \frac{1}{D}\left(\sum_{k}^{N}\frac{x^2_k}{\sigma^2_k} \sum_{k}^{N}\frac{y_k}{\sigma^2_k} - \sum_{k}^{N}\frac{x_k}{\sigma^2_k} \sum_{k}^{N}\frac{x_k y_k}{\sigma^2_k}\right) \\
    a_1 &= \frac{1}{D}\left(\sum_{k}^{N}\frac{1}{\sigma^2_k} \sum_{k}^{N}\frac{x_k y_k}{\sigma^2_k} - \sum_{k}^{N}\frac{x_k}{\sigma^2_k} \sum_{k}^{N}\frac{y_k}{\sigma^2_k}\right) \\
    D &= \sum_{k}^{N}\frac{1}{\sigma^2_k} \sum_{k}^{N}\frac{x^2_k}{\sigma^2_k} - \left(\sum_{k}^{N}\frac{x_k}{\sigma^2_k}\right)^2
\end{align}
Uncertainties of Parameters are:
\begin{align}
\sigma^2_{a_0} = \frac{1}{D} \sum_{k}^{N}\frac{x^2_j}{\sigma^2_j} \\
\sigma^2_{a_1} = \frac{1}{D} \sum_{k}^{N}\frac{1}{\sigma^2_j}
\end{align}

\subsection{Error Propagation}
\begin{align}
  \sigma^2_y = \sum_i^m \left(\frac{\partial f}{\partial x_i}\right)^2 \sigma^2_i
\end{align}



\section{Measurements}


\begin{table}[H]
\caption{\label{tab:yellow}%
Measurements of applied voltage $V$ and amplified photocurrent $I$ with their errors $\sigma_{V}$ and $\sigma_{I}$ for yellow color.
}
\begin{ruledtabular}
\begin{tabular}{cccc}
\textrm{\textit{$V$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$\sigma_{V}$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$I$}\hspace{1mm}[$\si{\milli\ampere}]$} &
\textrm{\textit{$\sigma_{I}$}\hspace{1mm}[$\si{\milli\ampere}]$}\\
\colrule
0.000          & 0.000               & 3.20            & 0.11                   \\
0.101          & 0.004               & 3.19            & 0.11                   \\
0.151          & 0.004               & 3.17            & 0.11                   \\
0.161          & 0.004               & 3.15            & 0.11                   \\
0.173          & 0.004               & 2.86            & 0.11                   \\
0.179          & 0.004               & 2.72            & 0.10                   \\
0.192          & 0.004               & 2.46            & 0.10                   \\
0.201          & 0.004               & 2.27            & 0.09                   \\
0.210          & 0.004               & 2.23            & 0.09                   \\
0.218          & 0.004               & 2.09            & 0.09                   \\
0.251          & 0.004               & 1.45            & 0.08                   \\
0.302          & 0.005               & 0.76            & 0.07                   \\
0.351          & 0.005               & 0.24            & 0.05                   \\
0.391          & 0.005               & -0.05           & 0.05                   \\
0.450          & 0.005               & -0.37           & 0.06                   \\
0.505          & 0.005               & -0.56           & 0.06                   \\
0.549          & 0.006               & -0.67           & 0.06                   \\
0.601          & 0.006               & -0.74           & 0.06                   \\
0.653          & 0.006               & -0.81           & 0.07                   \\
0.710          & 0.007               & -0.87           & 0.07                   \\
0.813          & 0.007               & -0.91           & 0.07                   \\
1.020          & 0.008               & -0.98           & 0.07                   \\
1.205          & 0.009               & -1.02           & 0.07                   \\
1.408          & 0.010               & -1.05           & 0.07                   \\
1.603          & 0.011               & -1.07           & 0.07                   \\
2.006          & 0.013               & -1.12           & 0.07                   \\
2.202          & 0.014               & -1.14           & 0.07                   \\
2.454          & 0.015               & -1.17           & 0.07                  
\end{tabular}  
\end{ruledtabular}
\end{table}


\begin{table}[H]
\caption{\label{tab:green}%
Measurements of applied voltage $V$ and amplified photocurrent $I$ with their errors $\sigma_{V}$ and $\sigma_{I}$ for green color.
}
\begin{ruledtabular}
\begin{tabular}{cccc}
\textrm{\textit{$V$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$\sigma_{V}$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$I$}\hspace{1mm}[$\si{\milli\ampere}]$} &
\textrm{\textit{$\sigma_{I}$}\hspace{1mm}[$\si{\milli\ampere}]$}\\
\colrule
0.000          & 0.000               & 3.15            & 0.11                 \\
0.116          & 0.003               & 3.16            & 0.11                 \\
0.217          & 0.004               & 3.17            & 0.11                 \\
0.306          & 0.005               & 3.17            & 0.11                 \\
0.315          & 0.005               & 3.17            & 0.11                 \\
0.348          & 0.005               & 3.15            & 0.11                 \\
0.352          & 0.005               & 3.05            & 0.11                 \\
0.358          & 0.005               & 2.91            & 0.11                 \\
0.367          & 0.005               & 2.80            & 0.11                 \\
0.377          & 0.005               & 2.45            & 0.10                 \\
0.390          & 0.005               & 2.05            & 0.09                 \\
0.399          & 0.005               & 1.82            & 0.09                 \\
0.416          & 0.005               & 1.36            & 0.08                 \\
0.431          & 0.005               & 1.02            & 0.07                 \\
0.443          & 0.005               & 0.75            & 0.07                 \\
0.452          & 0.005               & 0.57            & 0.06                 \\
0.460          & 0.005               & 0.40           & 0.06                 \\
0.479          & 0.005               & 0.07            & 0.05                 \\
0.489          & 0.005               & -0.07           & 0.05                 \\
0.502          & 0.005               & -0.24           & 0.05                 \\
0.527          & 0.006               & -0.55           & 0.05                 \\
0.549          & 0.006               & -0.74           & 0.06                 \\
0.572          & 0.006               & -0.92           & 0.07                 \\
0.602          & 0.006               & -1.11           & 0.07                 \\
0.654          & 0.006               & -1.33           & 0.08                 \\
0.756          & 0.007               & -1.60           & 0.08                 \\
0.847          & 0.007               & -1.73           & 0.08                 \\
0.963          & 0.008               & -1.84           & 0.09                 \\
1.122          & 0.009               & -1.90           & 0.09                 \\
1.247          & 0.009               & -1.93           & 0.09                 \\
1.428          & 0.010               & -1.98           & 0.09                 \\
1.614          & 0.011               & -2.06           & 0.09                 \\
1.825          & 0.012               & -2.09           & 0.09                 \\
2.024          & 0.013               & -2.14           & 0.09                 \\
2.190          & 0.014               & -2.16           & 0.09                 \\
2.495          & 0.015               & -2.22           & 0.09                    
\end{tabular}  
\end{ruledtabular}
\end{table}


\begin{table}[H]
\caption{\label{tab:cyan}%
Measurements of applied voltage $V$ and amplified photocurrent $I$ with their errors $\sigma_{V}$ and $\sigma_{I}$ for cyan color.
}
\begin{ruledtabular}
\begin{tabular}{cccc}
\textrm{\textit{$V$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$\sigma_{V}$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$I$}\hspace{1mm}[$\si{\milli\ampere}]$} &
\textrm{\textit{$\sigma_{I}$}\hspace{1mm}[$\si{\milli\ampere}]$}\\
\colrule
0.000          & 0.003               & 2.88            & 0.11                 \\
0.011          & 0.003               & 2.77            & 0.11                 \\
0.021          & 0.003               & 2.76            & 0.11                 \\
0.036          & 0.003               & 2.69            & 0.10                 \\
0.049          & 0.003               & 2.55            & 0.10                 \\
0.068          & 0.003               & 2.43            & 0.10                 \\
0.078          & 0.003               & 2.39            & 0.10                 \\
0.087          & 0.003               & 2.20            & 0.09                 \\
0.104          & 0.003               & 2.08            & 0.09                 \\
0.130          & 0.004               & 1.91            & 0.09                 \\
0.149          & 0.004               & 1.76            & 0.09                 \\
0.182          & 0.004               & 1.59            & 0.08                 \\
0.196          & 0.004               & 1.49            & 0.08                 \\
0.228          & 0.004               & 1.31            & 0.08                 \\
0.252          & 0.004               & 1.20            & 0.07                 \\
0.305          & 0.005               & 0.96            & 0.07                 \\
0.360          & 0.005               & 0.74            & 0.06                 \\
0.417          & 0.005               & 0.51            & 0.06                 \\
0.501          & 0.006               & 0.25            & 0.05                 \\
0.602          & 0.006               & 0.07            & 0.05                 \\
0.705          & 0.007               & -0.05           & 0.05                 \\
0.790          & 0.007               & -0.10           & 0.05                 \\
0.907          & 0.008               & -0.14           & 0.05                 \\
1.204          & 0.009               & -0.19           & 0.05                 \\
1.405          & 0.010               & -0.21           & 0.05                 \\
1.750          & 0.012               & -0.23           & 0.05                 \\
2.025          & 0.013               & -0.24           & 0.05                 \\
2.210          & 0.014               & -0.25           & 0.05                 \\
2.495          & 0.015               & -0.26           & 0.05                
\end{tabular}  
\end{ruledtabular}
\end{table}



\begin{table}[H]
\caption{\label{tab:blue}%
Measurements of applied voltage $V$ and amplified photocurrent $I$ with their errors $\sigma_{V}$ and $\sigma_{I}$ for blue color.
}
\begin{ruledtabular}
\begin{tabular}{cccc}
\textrm{\textit{$V$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$\sigma_{V}$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$I$}\hspace{1mm}[$\si{\milli\ampere}]$} &
\textrm{\textit{$\sigma_{I}$}\hspace{1mm}[$\si{\milli\ampere}]$}\\
\colrule
0.000    & 0.003               & 3.11            & 0.11                 \\
0.100    & 0.003               & 3.11            & 0.11                 \\
0.786    & 0.007               & 3.16            & 0.11                 \\
0.847    & 0.007               & 3.13            & 0.11                 \\
0.857    & 0.007               & 3.08            & 0.11                 \\
0.864    & 0.007               & 2.56            & 0.10                 \\
0.873    & 0.007               & 2.41            & 0.10                 \\
0.884    & 0.007               & 2.15            & 0.09                 \\
0.896    & 0.007               & 1.90            & 0.09                 \\
0.910    & 0.007               & 1.49            & 0.08                 \\
0.920    & 0.008               & 1.25            & 0.07                 \\
0.934    & 0.008               & 1.08            & 0.07                 \\
0.950    & 0.008               & 0.83            & 0.07                 \\
0.988    & 0.008               & 0.32            & 0.06                 \\
1.001    & 0.008               & 0.17            & 0.05                 \\
1.022    & 0.008               & -0.05           & 0.05                 \\
1.033    & 0.008               & -0.17           & 0.05                 \\
1.043    & 0.008               & -0.27           & 0.05                 \\
1.054    & 0.008               & -0.35           & 0.06                 \\
1.075    & 0.008               & -0.53           & 0.06                 \\
1.102    & 0.009               & -0.73           & 0.06                 \\
1.210    & 0.009               & -1.32           & 0.08                 \\
1.301    & 0.009               & -1.6            & 0.08                 \\
1.405    & 0.010               & -1.83           & 0.09                 \\
1.504    & 0.011               & -1.97           & 0.09                 \\
1.606    & 0.011               & -2.02           & 0.09                 \\
1.690    & 0.011               & -2.06           & 0.09                 \\
1.923    & 0.013               & -2.10           & 0.09                 \\
2.111    & 0.013               & -2.10           & 0.09                 \\
2.199    & 0.014               & -2.15           & 0.09                 \\
2.317    & 0.015               & -2.19           & 0.09                 \\
2.495    & 0.015               & -2.31           & 0.10                
\end{tabular}  
\end{ruledtabular}
\end{table}


\begin{table}[H]
\caption{\label{tab:violet}%
Measurements of applied voltage $V$ and amplified photocurrent $I$ with their errors $\sigma_{V}$ and $\sigma_{I}$ for violet color.
}
\begin{ruledtabular}
\begin{tabular}{cccc}
\textrm{\textit{$V$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$\sigma_{V}$}\hspace{1mm}[$\si{\volt}]$} &
\textrm{\textit{$I$}\hspace{1mm}[$\si{\milli\ampere}]$} &
\textrm{\textit{$\sigma_{I}$}\hspace{1mm}[$\si{\milli\ampere}]$}\\
\colrule
0.000          & 0.000               & 3.16            & 0.11                 \\
0.106          & 0.003               & 3.15            & 0.11                 \\
0.198          & 0.003               & 3.16            & 0.11                 \\
0.295          & 0.004               & 3.16            & 0.11                 \\
0.807          & 0.007               & 3.15            & 0.11                 \\
0.820          & 0.007               & 3.15            & 0.11                 \\
0.830          & 0.007               & 3.13            & 0.11                 \\
0.839          & 0.007               & 3.07            & 0.11                 \\
0.857          & 0.007               & 2.90            & 0.11                 \\
0.863          & 0.007               & 2.72            & 0.10                 \\
0.873          & 0.007               & 2.67            & 0.10                 \\
0.882          & 0.007               & 2.54            & 0.10                 \\
0.894          & 0.007               & 2.41            & 0.09                 \\
0.913          & 0.007               & 2.17            & 0.09                 \\
0.930          & 0.008               & 1.96            & 0.09                 \\
0.949          & 0.008               & 1.78            & 0.09                 \\
0.967          & 0.008               & 1.46            & 0.08                 \\
1.007          & 0.008               & 1.06            & 0.07                 \\
1.096          & 0.008               & 0.44            & 0.06                 \\
1.132          & 0.009               & 0.24            & 0.05                 \\
1.157          & 0.009               & 0.10            & 0.05                 \\
1.188          & 0.009               & -0.02           & 0.05                 \\
1.206          & 0.009               & -0.09           & 0.05                 \\
1.234          & 0.009               & -0.19           & 0.05                 \\
1.256          & 0.009               & -0.26           & 0.05                 \\
1.269          & 0.009               & -0.29           & 0.05                 \\
1.284          & 0.009               & -0.34           & 0.06                 \\
1.322          & 0.010               & -0.44           & 0.06                 \\
1.342          & 0.010               & -0.46           & 0.06                 \\
1.365          & 0.010               & -0.49           & 0.06                 \\
1.384          & 0.010               & -0.52           & 0.06                 \\
1.395          & 0.010               & -0.54           & 0.06                 \\
1.416          & 0.010               & -0.58           & 0.06                 \\
1.426          & 0.010               & -0.57           & 0.06                 \\
1.453          & 0.010               & -0.61           & 0.06                 \\
1.479          & 0.010               & -0.62           & 0.06                 \\
1.507          & 0.011               & -0.67           & 0.06                 \\
1.541          & 0.011               & -0.72           & 0.06                 \\
1.575          & 0.011               & -0.76           & 0.07                 \\
1.658          & 0.011               & -0.81           & 0.07                 \\
1.709          & 0.011               & -0.84           & 0.07                 \\
1.780          & 0.012               & -0.87           & 0.07                 \\
1.853          & 0.012               & -0.85           & 0.07                 \\
1.948          & 0.013               & -0.91           & 0.07                 \\
2.028          & 0.013               & -0.96           & 0.07                 \\
2.116          & 0.013               & -0.93           & 0.07                 \\
2.209          & 0.014               & -0.98           & 0.07                 \\
2.290          & 0.014               & -1.01           & 0.07                 \\
2.494          & 0.015               & -1.03           & 0.07                
\end{tabular}  
\end{ruledtabular}
\end{table}




\nocite{*}
\bibliography{report}

\end{document}
